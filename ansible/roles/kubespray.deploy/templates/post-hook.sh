#!/bin/sh

. {{ kubespray_virtualenv }}/bin/activate

cd {{ kubespray_repodir }}

LOCAL_OVERRIDES=
if [ -s "{{ kubespray_local_overrides_file }}" ]; then
    LOCAL_OVERRIDES="-e @{{ kubespray_local_overrides_file }}"
fi

exec ansible-playbook -b -v \
    -i {{ kubespray_inventory_file }} \
    cluster.yml \
    -e @{{ kubespray_overrides_file }} $LOCAL_OVERRIDES $EXTRA_OVERRIDES
