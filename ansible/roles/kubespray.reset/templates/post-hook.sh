#!/bin/sh

if [ ! -e {{ kubespray_virtualenv }}/bin/activate \
       -o ! -e {{ kubespray_repodir }} ]; then
    exit 0
fi

. {{ kubespray_virtualenv }}/bin/activate

cd {{ kubespray_repodir }}

LOCAL_OVERRIDES=
if [ -s "{{ kubespray_local_overrides_file }}" ]; then
    LOCAL_OVERRIDES="-e @{{ kubespray_local_overrides_file }}"
fi

exec ansible-playbook -b -v \
    -i {{ kubespray_inventory_file }} \
    reset.yml \
    -e @{{ kubespray_overrides_file }} -e reset_confirmation=yes -e nginx_config_dir=/tmp/nginx_config_dir $LOCAL_OVERRIDES $EXTRA_OVERRIDES
