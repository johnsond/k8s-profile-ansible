#!/usr/bin/env python

from lxml import etree as ET
import crypt
import random
import os.path
import sys

import geni.portal as portal
import geni.rspec.pg as pg
import geni.rspec.igext as ig
# Emulab specific extensions.
import geni.rspec.emulab as emulab
# Import the Emulab specific extensions.
import geni.rspec.emulab as emulab
# Import Emulab Ansible-specific extensions.
import geni.rspec.emulab.ansible
from geni.rspec.emulab.ansible import (Role, RoleBinding, Override, Playbook)


class EmulabEncrypt(pg.Resource):
    def __init__(self,name):
        super(EmulabEncrypt,self).__init__()
        self.name = name

    def _write(self, root):
        ns = "{%s}" % (pg.Namespaces.EMULAB,)
        el = ET.SubElement(root,"%spassword" % (ns,),attrib={'name':self.name})

HEAD_CMD = "sudo -u `geni-get user_urn | cut -f4 -d+` -Hi /bin/sh -c '%s /local/repository/emulab-ansible-bootstrap/head.sh >/local/logs/setup.log 2>&1'"
CLIENT_CMD = "sudo -u `geni-get user_urn | cut -f4 -d+` -Hi /bin/sh -c '%s /local/repository/emulab-ansible-bootstrap/client.sh >/local/logs/setup.log 2>&1'"

#
# Create our in-memory model of the RSpec -- the resources we're going
# to request in our experiment, and their configuration.
#
request = pg.Request()

#
# This geni-lib script is designed to run in the CloudLab Portal.
#
pc = portal.Context()

#
# Add some custom ansible roles to the rspec; we will bind them to nodes later.
#
request.addRole(
    Role("kubespray.node",auto=False))
request.addRole(
    Role("kubespray",path="ansible",group="kubespray.manager",playbooks=[
        Playbook("kubespray",path="kubespray.yml",become="root",
                 post_hook="/local/setup/kubespray/post-hook.sh"),
        Playbook("kubespray_post",path="kubespray_post.yml",become="root")]))
request.addRole(
    Role("kubespray.reset",path="ansible",group="kubespray.manager",auto=False,playbooks=[
        Playbook("kubespray_reset",path="kubespray_reset.yml",become="root",
                 post_hook="/local/setup/kubespray/reset-post-hook.sh")]))

#
# Define some parameters.
#
pc.defineParameter(
    "nodeCount","Number of Nodes",
    portal.ParameterType.INTEGER,1,
    longDescription="Number of nodes in your kubernetes cluster.  Should be either 1, or >= 3.  If only 1, that node takes on the control plane, worker node, and etcd roles.  If >= 3, the first two nodes are control plane; the first three nodes are etcd; and the remaining nodes are workers.")
pc.defineParameter(
    "nodeType","Hardware Type",
    portal.ParameterType.NODETYPE,"",
    longDescription="A specific hardware type to use for each node.  Cloudlab clusters all have machines of specific types.  When you set this field to a value that is a specific hardware type, you will only be able to instantiate this profile on clusters with machines of that type.  If unset, when you instantiate the profile, the resulting experiment may have machines of any available type allocated.")
pc.defineParameter(
    "linkSpeed","Experiment Link Speed",
    portal.ParameterType.INTEGER,0,
    [(0,"Any"),(1000000,"1Gb/s"),(10000000,"10Gb/s"),(25000000,"25Gb/s"),(40000000,"40Gb/s"),(100000000,"100Gb/s")],
    longDescription="A specific link speed to use for each link/LAN.  All experiment network interfaces will request this speed.")
pc.defineParameter(
    "ansibleDisablePlaybookAutomation","Disable Ansible Automation",
    portal.ParameterType.BOOLEAN,False,
    advanced=True,
    longDescription="Disable Ansible playbook execution automation.  If this option is set, Ansible will be installed on the head node, and clients will be configured to be managed, and the head node will generate the playbook automation scripts -- but it will not run them.  You can do that by logging in to the head node and running `/local/repository/emulab-ansible-bootstrap/head.sh` manually.")
pc.defineParameter(
    "diskImage","Disk Image",
    portal.ParameterType.STRING,
    "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU22-64-STD",
    advanced=True,
    longDescription="An image URN or URL that every node will run.")
pc.defineParameter(
    "multiplexLans", "Multiplex Networks",
    portal.ParameterType.BOOLEAN,False,
    longDescription="Multiplex any networks over physical interfaces using VLANs.  Some physical machines have only a single experiment network interface, so if you want multiple links/LANs, you have to enable multiplexing.  Currently, if you select this option.",
    advanced=True)
pc.defineStructParameter(
    "sharedVlans","Add Shared VLAN",[],
    multiValue=True,itemDefaultValue={},min=0,max=None,
    members=[
        portal.Parameter(
            "createConnectableSharedVlan","Create Connectable Shared VLAN",
            portal.ParameterType.BOOLEAN,False,
            longDescription="Create a placeholder, connectable shared VLAN stub and 'attach' the first node to it.  You can use this during the experiment to connect this experiment interface to another experiment's shared VLAN."),
        portal.Parameter(
            "createSharedVlan","Create Shared VLAN",
            portal.ParameterType.BOOLEAN,False,
            longDescription="Create a new shared VLAN with the name above, and connect the first node to it."),
        portal.Parameter(
            "connectSharedVlan","Connect to Shared VLAN",
            portal.ParameterType.BOOLEAN,False,
            longDescription="Connect an existing shared VLAN with the name below to the first node."),
        portal.Parameter(
            "sharedVlanName","Shared VLAN Name",
            portal.ParameterType.STRING,"",
            longDescription="A shared VLAN name (functions as a private key allowing other experiments to connect to this node/VLAN), used when the 'Create Shared VLAN' or 'Connect to Shared VLAN' options above are selected.  Must be fewer than 32 alphanumeric characters."),
        portal.Parameter(
            "sharedVlanAddress","Shared VLAN IP Address",
            portal.ParameterType.STRING,"10.254.254.1",
            longDescription="Set the IP address for the shared VLAN interface.  Make sure to use an unused address within the subnet of an existing shared vlan!"),
        portal.Parameter(
            "sharedVlanNetmask","Shared VLAN Netmask",
            portal.ParameterType.STRING,"255.255.255.0",
            longDescription="Set the subnet mask for the shared VLAN interface, as a dotted quad.")])
pc.defineStructParameter(
    "datasets","Datasets",[],
    multiValue=True,itemDefaultValue={},min=0,max=None,
    members=[
        portal.Parameter(
            "urn","Dataset URN",
            portal.ParameterType.STRING,"",
            longDescription="The URN of an *existing* remote dataset (a remote block store) that you want attached to the node you specified (defaults to the first node).  The block store must exist at the cluster at which you instantiate the profile."),
        portal.Parameter(
            "mountNode","Dataset Mount Node",
            portal.ParameterType.STRING,"node-0",
            longDescription="The node on which you want your remote block store mounted; defaults to the first node."),
        portal.Parameter(
            "mountPoint","Dataset Mount Point",
            portal.ParameterType.STRING,"/dataset",
            longDescription="The mount point at which you want your remote dataset mounted.  Be careful where you mount it -- something might already be there (i.e., /storage is already taken).  Note also that this option requires a network interface, because it creates a link between the dataset and the node where the dataset is available.  Thus, just as for creating extra LANs, you might need to select the Multiplex Flat Networks option, which will also multiplex the blockstore link here."),
        portal.Parameter(
            "mode","Dataset Mount Mode (R/O, RW clone, persistent RW)",
            portal.ParameterType.STRING, "readonly", [("readonly", "Read Only"),("rwclone","Read/Write Clone"),("rwpersist","Persistent Read/Write")],
            longDescription="Mode to mount the dataset with. Readonly is the default. RW clone allows for read/write, but the changes do not persist after experiment termination. Persistent RW causes changes to persist beyond the experiment.")])

pc.defineParameter(
    "kubesprayRepo","Kubespray Git Repository",
    portal.ParameterType.STRING,
    "https://github.com/kubernetes-incubator/kubespray.git",
    longDescription="Do not change this value unless you know what you are doing!  Changing would only be necessary if you have a modified fork of Kubespray.  This must be a publicly-accessible repository.",
    advanced=True)
request.addOverride(
    Override("kubespray_repo",source="parameter",source_name="emulab.net.parameter.kubesprayRepo",on_empty=False))
pc.defineParameter(
    "kubesprayVersion","Kubespray Version",
    portal.ParameterType.STRING,"release-2.21",
    longDescription="A tag or commit-ish value; we will run `git checkout <value>`.  The default value is the most recent stable value we have tested.  You should only change this if you need a new feature only available on `master`, or an old feature from a prior release.  We support versions back to release-2.13 only.  Ubuntu 22 supports only release-2.20 and greater.  You will need to use Ubuntu 20 for anything prior to that.",
    advanced=True)
request.addOverride(
    Override("kubespray_version",source="parameter",source_name="emulab.net.parameter.kubesprayVersion",on_empty=False))
pc.defineParameter(
    "kubeVersion","Kubernetes Version",
    portal.ParameterType.STRING,"",
    longDescription="A specific release of Kubernetes to install (e.g. v1.16.3); if left empty, Kubespray will choose its current stable version and install that.  You can check for Kubespray-known releases at https://github.com/kubernetes-sigs/kubespray/blob/release-2.16/roles/download/defaults/main.yml (or if you're using a different Kubespray release, choose the corresponding feature release branch in that URL).  You can use unsupported or unknown versions, however, as long as the binaries actually exist.",
    advanced=True)
request.addOverride(
    Override("kube_version",source="parameter",source_name="emulab.net.parameter.kubeVersion",on_empty=False))
pc.defineParameter(
    "helmVersion","Helm Version",
    portal.ParameterType.STRING,"",
    longDescription="A specific release of Helm to install (e.g. v2.12.3); if left empty, Kubespray will choose its current stable version and install that.  Note that the version you pick must exist as a tag in this Docker image repository: https://hub.docker.com/r/lachlanevenson/k8s-helm/tags .",
    advanced=True)
request.addOverride(
    Override("helm_version",source="parameter",source_name="emulab.net.parameter.helmVersion",on_empty=False))
pc.defineParameter(
    "containerManager","Container Manager",
    portal.ParameterType.STRING,"docker",
    [("docker","docker"),("containerd","containerd")],
    longDescription="The container manager to use; either docker or containerd.",
    advanced=True)
request.addOverride(
    Override("container_manager",source="parameter",source_name="emulab.net.parameter.containerManager",on_empty=False))
pc.defineParameter(
    "dockerVersion","Docker Version",
    portal.ParameterType.STRING,"",
    longDescription="A specific Docker version to install; if left empty, Kubespray will choose its current stable version and install that.  As explained in the Kubespray documentation (https://github.com/kubernetes-sigs/kubespray/blob/master/docs/vars.md), this value must be one of those listed at, e.g. https://github.com/kubernetes-sigs/kubespray/blob/release-2.20/roles/container-engine/docker/vars/ubuntu.yml .",
    advanced=True)
request.addOverride(
    Override("docker_version",source="parameter",source_name="emulab.net.parameter.dockerVersion",on_empty=False))
pc.defineParameter(
    "dockerOptions","Dockerd Options",
    portal.ParameterType.STRING,"",
    longDescription="Extra command-line options to pass to dockerd.  The most common option is probably an --insecure-registry .",
    advanced=True)
request.addOverride(
    Override("docker_options",source="parameter",source_name="emulab.net.parameter.dockerOptions",on_empty=False))
pc.defineParameter(
    "doLocalRegistry","Create Private, Local Registry",
    portal.ParameterType.BOOLEAN,True,
    longDescription="Create a private Docker registry on the kube master, and expose it on the (private) management IP address, port 5000, and configure Kubernetes to be able to use it (--insecure-registry).  This is nearly mandatory for some development workflows, so it is on by default.",
    advanced=True)
request.addOverride(
    Override("kubespray_do_local_registry",source="parameter",source_name="emulab.net.parameter.doLocalRegistry"))
pc.defineParameter(
    "kubeNetworkPlugin","Kubernetes Network Plugin",
    portal.ParameterType.STRING,"calico",
    [("calico","Calico"),("flannel","Flannel"),("weave","Weave"),
     ("canal","Canal")],
    longDescription="Choose the primary kubernetes network plugin.",
    advanced=True)
request.addOverride(
    Override("kube_network_plugin",source="parameter",source_name="emulab.net.parameter.kubeNetworkPlugin",on_empty=False))
pc.defineParameter(
    "kubeEnableMultus","Enable Multus Network Meta Plugin",
    portal.ParameterType.BOOLEAN,False,
    longDescription="Select to enable the Multus (https://github.com/kubernetes-sigs/kubespray/blob/master/docs/multus.md) CNI meta plugin.  Multus provides multiple network interface support to pods.",
    advanced=True)
request.addOverride(
    Override("kube_network_plugin_multus",source="parameter",source_name="emulab.net.parameter.kubeEnableMultus"))
pc.defineParameter(
    "kubeProxyMode","Kube Proxy Mode",
    portal.ParameterType.STRING,"ipvs",
    [("iptables","iptables"),("ipvs","ipvs")],
    longDescription="Choose the mode for kube-proxy (comparison: https://www.projectcalico.org/comparing-kube-proxy-modes-iptables-or-ipvs/).",
    advanced=True)
request.addOverride(
    Override("kube_proxy_mode",source="parameter",source_name="emulab.net.parameter.kubeProxyMode",on_empty=False))
pc.defineParameter(
    "kubePodsSubnet","Kubernetes Pods Subnet",
    portal.ParameterType.STRING,"192.168.0.0/17",
    longDescription="The subnet containing pod addresses.",
    advanced=True)
request.addOverride(
    Override("kube_pods_subnet",source="parameter",source_name="emulab.net.parameter.kubePodsSubnet",on_empty=False))
pc.defineParameter(
    "kubeServiceAddresses","Kubernetes Service Addresses",
    portal.ParameterType.STRING,"192.168.128.0/17",
    longDescription="The subnet containing service addresses.",
    advanced=True)
request.addOverride(
    Override("kube_service_addresses",source="parameter",source_name="emulab.net.parameter.kubeServiceAddresses",on_empty=False))
pc.defineParameter(
    "kubeDoMetalLB","Kubespray Enable MetalLB",
    portal.ParameterType.BOOLEAN,True,
    longDescription="We enable MetalLB by default, so that users can use an \"external\" load balancer service type.  You need at least one public IP address for this option because it doesn't make sense without one.",
    advanced=True)
request.addOverride(
    Override("kubespray_do_metallb",source="parameter",source_name="emulab.net.parameter.kubeDoMetalLB"))
pc.defineParameter(
    "publicIPCount", "Number of public IP addresses",
    portal.ParameterType.INTEGER,1,
    longDescription="Set the number of public IP addresses you will need for externally-published services (e.g., via a load balancer like MetalLB.",
    advanced=True)
pc.defineParameter(
    "kubeFeatureGates","Kubernetes Feature Gate List",
    portal.ParameterType.STRING,"",
    longDescription="A []-enclosed, comma-separated list of features.  For instance, `[SCTPSupport=true]`.",
    advanced=True)
request.addOverride(
    Override("kube_feature_gates",source="parameter",source_name="emulab.net.parameter.kubeFeatureGates",on_empty=False))
pc.defineParameter(
    "kubeletCustomFlags","Kubelet Custom Flags List",
    portal.ParameterType.STRING,"",
    longDescription="A []-enclosed, comma-separated list of flags.  For instance, `[--allowed-unsafe-sysctls=net.*]`.",
    advanced=True)
request.addOverride(
    Override("kubelet_custom_flags",source="parameter",source_name="emulab.net.parameter.kubeletCustomFlags",on_empty=False))
pc.defineParameter(
    "kubeletMaxPods","Kubelet Max Pods",
    portal.ParameterType.INTEGER,0,
    longDescription="An integer max pods limit; 0 allows Kubernetes to use its default value (currently is 110; see https://kubespray.io/#/docs/vars and look for `kubelet_max_pods`).  Do not change this unless you know what you are doing.",
    advanced=True)
request.addOverride(
    Override("kubelet_max_pods",source="parameter",source_name="emulab.net.parameter.kubeletMaxPods",on_empty=False))
pc.defineParameter(
    "kubeAllWorkers","Kube Control Plane nodes are also Workers",
    portal.ParameterType.BOOLEAN,False,
    longDescription="Allow the kube control plane nodes to be a worker in the multi-node case (always true for single-node clusters); disabled by default.",
    advanced=True)
request.addOverride(
    Override("kubespray_all_workers",source="parameter",source_name="emulab.net.parameter.kubeAllWorkers"))
pc.defineParameter(
    "sslCertType","SSL Certificate Type",
    portal.ParameterType.STRING,"selfsigned",
    [("selfsigned","Self-Signed"),("letsencrypt","Let's Encrypt")],
    advanced=True,
    longDescription="Choose an SSL Certificate strategy.  By default, we generate self-signed certificates, and only use them for a reverse web proxy to allow secure remote access to the Kubernetes Dashboard.  However, you may choose `None` if you prefer to arrange remote access differently (e.g. ssh port forwarding).  You may also choose to use Let's Encrypt certificates whose trust root is accepted by all modern browsers.")
request.addOverride(
    Override("kubespray__emulab_ssl_cert_type",source="parameter",source_name="emulab.net.parameter.sslCertType",on_empty=False))
pc.defineParameter(
    "nfsAsync","Export NFS volume async",
    portal.ParameterType.BOOLEAN,True,
    longDescription="Force the default NFS volume to be exported `async`.  When enabled, clients will only be given asynchronous write behavior even if they request sync or write with sync flags.  This is dangerous, but some applications that rely on persistent storage cannot be configured to use more helpful sync options (e.g., fsync instead of O_DIRECT).  It will give you the absolute best performance, however.",
    advanced=True)
request.addOverride(
    Override("kubespray__emulab_nfs_async",source="parameter",source_name="emulab.net.parameter.nfsAsync"))

# Map some passwords to ansible overrides.
request.addOverride(
    Override("kubespray_admin_password",source="password",source_name="adminPass"))

#
# Get any input parameter values that will override our defaults.
#
params = pc.bindParameters()

if params.publicIPCount > 8:
    perr = portal.ParameterWarning(
        "You cannot request more than 8 public IP addresses, at least not without creating your own modified version of this profile!",
        ["publicIPCount"])
    pc.reportWarning(perr)
if params.kubeDoMetalLB and params.publicIPCount < 1:
    perr = portal.ParameterWarning(
        "If you enable MetalLB, you must request at least one public IP address!",
        ["kubeDoMetalLB","publicIPCount"])
    pc.reportWarning(perr)

i = 0
for x in params.sharedVlans:
    n = 0
    if x.createConnectableSharedVlan:
        n += 1
    if x.createSharedVlan:
        n += 1
    if x.connectSharedVlan:
        n += 1
    if n > 1:
        err = portal.ParameterError(
            "Must choose only a single shared vlan operation (create, connect, create connectable)",
        [ 'sharedVlans[%d].createConnectableSharedVlan' % (i,),
          'sharedVlans[%d].createSharedVlan' % (i,),
          'sharedVlans[%d].connectSharedVlan' % (i,) ])
        pc.reportError(err)
    if n == 0:
        err = portal.ParameterError(
            "Must choose one of the shared vlan operations: create, connect, create connectable",
        [ 'sharedVlans[%d].createConnectableSharedVlan' % (i,),
          'sharedVlans[%d].createSharedVlan' % (i,),
          'sharedVlans[%d].connectSharedVlan' % (i,) ])
        pc.reportError(err)
    i += 1

#
# Give the library a chance to return nice JSON-formatted exception(s) and/or
# warnings; this might sys.exit().
#
pc.verifyParameters()

#
# General kubernetes instruction text.
#
kubeInstructions = \
  """
## Waiting for your Experiment to Complete Setup

Once the initial phase of experiment creation completes (disk load and node configuration), the profile's setup scripts begin the complex process of installing software according to profile parameters, so you must wait to access software resources until they complete.  The Kubernetes dashboard link will not be available immediately.  There are multiple ways to determine if the scripts have finished.
  - First, you can watch the experiment status page: the overall State will say \"booted (startup services are still running)\" to indicate that the nodes have booted up, but the setup scripts are still running.
  - Second, the Topology View will show you, for each node, the status of the startup command on each node (the startup command kicks off the setup scripts on each node).  Once the startup command has finished on each node, the overall State field will change to \"ready\".  If any of the startup scripts fail, you can mouse over the failed node in the topology viewer for the status code.
  - Third, the profile configuration scripts send emails: one to notify you that profile setup has started, and another notify you that setup has completed.
  - Finally, you can view [the profile setup script logfile](http://{host-node-0}:7999/logs/setup.log) as the setup scripts run.  Use the `admin` username and the automatically-generated random password `{password-adminPass}` .  This URL is available a few minutes after profile setup scripts begin work.

## Kubernetes credentials and dashboard access

Once the profile's scripts have finished configuring software in your experiment, you'll be able to visit [the Kubernetes Dashboard WWW interface](https://{host-node-0}:8080/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/#!/login) (approx. 10-15 minutes for the Kubernetes portion alone).

The easiest login option is to use token authentication.  (Basic auth is configured if available, for older kubernetes versions, username `admin` password `{password-adminPass}`.  You may also supply a kubeconfig file, but we don't provide one that includes a secret by default, so you would have to generate that.)

For `token` authentication: copy the token from http://{host-node-0}:7999/kubespray/admin-token.txt (username `admin`, password `{password-adminPass}`) (this file is located on `node-0` in `/local/setup/admin-token.txt`).

(To provide secure dashboard access, we run a `kube-proxy` instance that listens on localhost:8888 and accepts all incoming hosts, and export that via nginx proxy listening on `{host-node-0}:8080`.  We also create an `admin` `serviceaccount` in the `default` namespace, and that is the serviceaccount associated with the token auth option mentioned just above.)

Kubernetes credentials are in `~/.kube/config`, or in `/root/.kube/config`, as you'd expect.

## Changing your Kubernetes deployment

The profile's setup scripts are automatically installed on each node in `/local/repository`, and all of the Kubernetes installation is triggered from `node-0`.  The scripts execute as your uid, and keep state and downloaded files in `/local/setup/`.  The scripts write copious logfiles in that directory; so if you think there's a problem with the configuration, you could take a quick look through these logs on the `node-0` node.  The primary logfile is `/local/logs/setup.log`.

Kubespray is a collection of Ansible playbooks, so you can make changes to the deployed kubernetes cluster, or even destroy and rebuild it (although you would then lose any of the post-install configuration we do in `/local/repository/setup-kubernetes-extra.sh`).  The `/local/repository/setup-kubespray.sh` script installs Ansible inside a Python 3 `virtualenv` (in `/local/setup/kubespray-virtualenv` on `node-0`).  A `virtualenv` (or `venv`) is effectively a separate part of the filesystem containing Python libraries and scripts, and a set of environment variables and paths that restrict its user to those Python libraries and scripts.  To modify your cluster's configuration in the Kubespray/Ansible way, you can run commands like these (as your uid):

1. "Enter" (or access) the `virtualenv`: `. /local/setup/kubespray-virtualenv/bin/activate`
2. Leave (or remove the environment vars from your shell session) the `virtualenv`: `deactivate`
3. Destroy your entire kubernetes cluster: `ansible-playbook -i /local/setup/inventories/emulab/inventory.ini /local/setup/kubespray/remove-node.yml -b -v --extra-vars "node=node-0,node-1,node-2"`
   (note that you would want to supply the short names of all nodes in your experiment)
4. Recreate your kubernetes cluster: `ansible-playbook -i /local/setup/inventories/emulab/inventory.ini /local/setup/kubespray/cluster.yml -b -v`

To change the Ansible and playbook configuration, you can start reading Kubespray documentation:
  - https://github.com/kubernetes-sigs/kubespray/blob/master/docs/getting-started.md
  - https://github.com/kubernetes-sigs/kubespray
  - https://kubespray.io/
"""

#
# Customizable area for forks.
#
tourDescription = \
  "This profile creates a Kubernetes cluster with [Kubespray]().  When you click the Instantiate button, you'll be presented with a list of parameters that you can change to control what your Kubernetes cluster will look like; read the parameter documentation on that page (or in the Instructions)."

tourInstructions = kubeInstructions

#
# Setup the Tour info with the above description and instructions.
#  
tour = ig.Tour()
tour.Description(ig.Tour.TEXT,tourDescription)
tour.Instructions(ig.Tour.MARKDOWN,tourInstructions)
request.addTour(tour)

datalans = []

if params.nodeCount > 1:
    lan_name = "datalan-1"
    datalan = pg.LAN(lan_name)
    if params.linkSpeed > 0:
        datalan.bandwidth = int(params.linkSpeed)
    if params.multiplexLans:
        datalan.link_multiplexing = True
        datalan.best_effort = True
        datalan.type = "vlan"
    datalans.append(datalan)
    request.addOverride(Override("kubespray_lan_name",value=lan_name))

nodes = dict({})

sharedvlans = []
envstring = ""
if params.ansibleDisablePlaybookAutomation:
    envstring = "EMULAB_ANSIBLE_NOAUTO=1 "
for i in range(0,params.nodeCount):
    nodename = "node-%d" % (i,)
    node = pg.RawPC(nodename)
    if i == 0:
        node.bindRole(RoleBinding("kubespray"))
    else:
        node.bindRole(RoleBinding("kubespray.node"))
    if params.nodeType:
        node.hardware_type = params.nodeType
    if params.diskImage:
        node.disk_image = params.diskImage
    j = 0
    for datalan in datalans:
        iface = node.addInterface("if%d" % (j,))
        datalan.addInterface(iface)
        j += 1
    if i == 0:
        node.addService(pg.Execute(shell="sh",command=HEAD_CMD % (envstring,)))
    else:
        node.addService(pg.Execute(shell="sh",command=CLIENT_CMD % (envstring,)))
    nodes[nodename] = node
    if i == 0:
        k = 0
        for x in params.sharedVlans:
            iface = node.addInterface("ifSharedVlan%d" % (k,))
            if x.sharedVlanAddress:
                iface.addAddress(
                    pg.IPv4Address(x.sharedVlanAddress,x.sharedVlanNetmask))
            sharedvlan = pg.Link('shared-vlan-%d' % (k,))
            sharedvlan.addInterface(iface)
            if x.createConnectableSharedVlan:
                sharedvlan.enableSharedVlan()
            else:
                if x.createSharedVlan:
                    sharedvlan.createSharedVlan(x.sharedVlanName)
                else:
                    sharedvlan.connectSharedVlan(x.sharedVlanName)
            if params.multiplexLans:
                sharedvlan.link_multiplexing = True
                sharedvlan.best_effort = True
            sharedvlans.append(sharedvlan)
            k += 1

#
# Add the dataset(s), if requested.
#
bsnodes = []
bslinks = []
i = 0
for x in params.datasets:
    if not x.urn:
        err = portal.ParameterError(
            "Must provide a non-null dataset URN",
            [ 'datasets[%d].urn' % (i,) ])
        pc.reportError(err)
        pc.verifyParameters()
    if x.mountNode not in nodes:
        perr = portal.ParameterError(
            "The node on which you mount your dataset must exist, and does not.",
            [ 'datasets[%d].mountNode' % (i,) ])
        pc.reportError(perr)
        pc.verifyParameters()
    bsn = nodes[x.mountNode]
    myintf = bsn.addInterface("ifbs%d" % (i,))
    bsnode = ig.RemoteBlockstore("bsnode-%d" % (i,),x.mountPoint)
    bsintf = bsnode.interface
    bsnode.dataset = x.urn
    if x.mode == "readonly":
        bsnode.readonly = True
    elif x.mode == "rwclone":
        bsnode.rwclone = True
    bsnodes.append(bsnode)

    bslink = pg.Link("bslink-%d" % (i,))
    bslink.addInterface(myintf)
    bslink.addInterface(bsintf)
    bslink.best_effort = True
    bslink.vlan_tagging = True
    bslinks.append(bslink)

for nname in nodes.keys():
    request.addResource(nodes[nname])
for datalan in datalans:
    request.addResource(datalan)
for x in sharedvlans:
    request.addResource(x)
for x in bsnodes:
    request.addResource(x)
for x in bslinks:
    request.addResource(x)

request.addResource(EmulabEncrypt("adminPass"))

#
# Grab a few public IP addresses.
#
apool = ig.AddressPool("node-0",params.publicIPCount)
request.addResource(apool)

pc.printRequestRSpec(request)
